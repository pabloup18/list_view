package com.example.list;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

import modelo.Produto;

public class cadastro extends AppCompatActivity {
    private final int RESULT_CODE_NOVO_PRODUTO = 10;
    private final int RESULT_CODE_PRODUTO_EDITADO = 11;

    private boolean edicao = false;
    private int id = 0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cadastro);
        setTitle("Cadastro produto");
        carregarProduto();
    }

    private void carregarProduto(){
        Intent intent = getIntent();
        if (intent != null && intent.getExtras() != null && intent.getExtras().get("produtoEdicao") != null){
            Produto produto = (Produto) intent.getExtras().get("produtoEdicao");
            EditText etnome = findViewById(R.id.ET_nome);
            EditText etvalor = findViewById(R.id.ET_valor);
            etnome.setText(produto.getNome());
            etvalor.setText(String.valueOf(produto.getValor()));
            edicao = true;
            id = produto.getId();
        }
    }

    public void onClickVoltar(View v){
        finish();
    }

    public void onClicSalvar(View v){
        EditText etnome = findViewById(R.id.ET_nome);
        EditText etvalor = findViewById(R.id.ET_valor);

        String nome = etnome.getText().toString();
        Float valor = Float.parseFloat(etvalor.getText().toString());

        Produto produto = new Produto(id, nome, valor);
        Intent intent = new Intent();

        if (edicao){
            intent.putExtra("produtoEditado", produto);
            setResult(RESULT_CODE_PRODUTO_EDITADO, intent);
        }else {
            intent.putExtra("novoProduto", produto);
            setResult(RESULT_CODE_NOVO_PRODUTO, intent);
        }
            finish();
    }
}